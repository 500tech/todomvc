import 'assets/stylesheets/base.scss';
import 'assets/stylesheets/index.scss';

import { render } from 'react-dom';

const App = () => (
  <div>Our ReactJS app goes here</div>
);

render(
  <App />,
  document.getElementById('app')
);
